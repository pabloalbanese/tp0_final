package ar.fiuba.tdd.tp0;

public class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
	  public void uncaughtException(Thread t, Throwable e) {
	     throw new IllegalArgumentException();
	   }
	}