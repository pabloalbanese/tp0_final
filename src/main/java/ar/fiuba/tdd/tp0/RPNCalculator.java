package ar.fiuba.tdd.tp0;

import java.util.ArrayList;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class RPNCalculator {

	private static String OPERATOR_SUM = "+";
	private static String OPERATOR_SUBTRACTION = "-";
	private static String OPERATOR_MULTIPLICATION = "*";
	private static String OPERATOR_DIVISION = "/";
	private static String OPERATOR_DOUBLESUM = "++";
	private static String OPERATOR_DOUBLESUBTRACTION = "--";
	private static String OPERATOR_DOUBLEMULTIPLICATION = "**";
	private static String OPERATOR_DOUBLEDIVISION = "//";
	private static String OPERATOR_MOD = "MOD";
	private static ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
	
	private int i;
	private String[] expressions;
	ArrayList<Float> numbers;
	
	public float eval(String expression) {
		Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
		
		if (expression != null && !expression.equals("")) {
			expressions = expression.split(" ");
			numbers = new ArrayList<Float>();
			i = 0;
			int old_i = i;
			while (i < expressions.length) {
				old_i = i;
				i = eval_numbers();
				try {
					i = eval_simbols();
				} catch (Exception e) {
					throw new IllegalArgumentException();
				}
				if(old_i == i)
					throw new IllegalArgumentException();
			}
			return (Float) numbers.get(0);
		} else
			throw new IllegalArgumentException();
	}
	
	private int eval_numbers() {
		while (expressions[i].matches("[-+]?\\d*\\.?\\d+")) {
			numbers.add(Float.parseFloat(expressions[i]));
			i++;
		}
		return i;
	}

	private int eval_simbols() throws NumberFormatException, ScriptException {
		while (i < expressions.length 
				&& (expressions[i].contains(OPERATOR_SUM)
					|| expressions[i].contains(OPERATOR_SUBTRACTION)
					|| expressions[i].contains(OPERATOR_MULTIPLICATION)
					|| expressions[i].contains(OPERATOR_DIVISION)
					|| expressions[i].contains(OPERATOR_MOD))) {
			i = eval_singleOperation(OPERATOR_SUM);
			i = eval_singleOperation(OPERATOR_SUBTRACTION);
			i = eval_singleOperation(OPERATOR_MULTIPLICATION);
			i = eval_singleOperation(OPERATOR_DIVISION);
			i = eval_mod(OPERATOR_MOD);
			i = eval_dobleOperationWithOneNumber(OPERATOR_DIVISION);
			i = eval_doubleOperation(OPERATOR_DOUBLESUM, OPERATOR_SUM);
			i = eval_doubleOperation(OPERATOR_DOUBLESUBTRACTION, OPERATOR_SUBTRACTION);
			i = eval_doubleOperation(OPERATOR_DOUBLEMULTIPLICATION, OPERATOR_MULTIPLICATION);
			i = eval_doubleOperation(OPERATOR_DOUBLEDIVISION, OPERATOR_DIVISION);
		}
		return i;
	}

	private void eval_setArray(Float result) {
		for (int j = 4; j >= 1; j--)
			numbers.remove(numbers.size() - j);
		numbers.add(result);
	}
	
	private int eval_dobleOperationWithOneNumber(String operation) {
		while ( i < expressions.length
				&& numbers.size() < 2 
				&& (expressions[i].equals(OPERATOR_DOUBLESUM) || expressions[i].equals(OPERATOR_DOUBLESUBTRACTION) 
				|| expressions[i].equals(OPERATOR_DOUBLEMULTIPLICATION) || expressions[i].equals(OPERATOR_DOUBLEDIVISION)) ) {
			Float result = numbers.get(numbers.size() - 1);

			numbers.remove(numbers.size() - 1);
			numbers.add(result);
			i++;
		}
		return i;
	}

	private int eval_doubleOperation(String Operator, String operator2) throws NumberFormatException, ScriptException {
		while (i < expressions.length && expressions[i].equals(Operator)) {
		    
		    int size = numbers.size();
		    String doubleOperation = numbers.get(size - 4) + operator2 + numbers.get(size - 3) + operator2 + numbers.get(size - 2) + operator2 + numbers.get(size - 1);
			eval_setArray(Float.parseFloat(engine.eval(doubleOperation).toString()));
			i++;
		}
		return i;
	}

	private int eval_mod(String Operator) {
		while (i < expressions.length && expressions[i].equals(OPERATOR_MOD)) {
		    Float number = numbers.get(numbers.size() - 2) % numbers.get(numbers.size() - 1);
			eval_setArrayForBinaryOperation(number);
			i++;
		}
		return i;
	}
	
	private int eval_singleOperation(String Operator) throws NumberFormatException, ScriptException {
		while (i < expressions.length && expressions[i].equals(Operator)) {
		    String singleOperation = numbers.get(numbers.size() - 2) + " " + Operator + " " + numbers.get(numbers.size() - 1);
			eval_setArrayForBinaryOperation(Float.parseFloat(engine.eval(singleOperation).toString()));
			i++;
		}
		return i;
	}

	private void eval_setArrayForBinaryOperation(Float result) {
		numbers.remove(numbers.size() - 2);
		numbers.remove(numbers.size() - 1);
		numbers.add(result);
	}

	public static void main(String[] args) {
		RPNCalculator calculator = new RPNCalculator();
		System.out.println(calculator.eval("1 2 + 3 4 + 5 6 + * *"));
	}
}